# wrc-projects summary

| Project | HW| GW | SW (soft-CPU) | SW (Linux) | Priority | Complexity | WRC dev plan |
|------------------------------------------------------------------------|-----|-----|-----|-----|-----|-----|-----|
| [WRPC: v5.0 support for all boards for which v4.2 is supported](#wrpc-v50-support-for-all-boards-for-which-v42-is-supported)            |     |  Y  |  Y  |  ?  |
| [WR starting kit: update the current version based on SPECs](#wr-starting-kit-update-the-current-version-based-on-specs)             |     |  Y  |  Y  |  Y  |
| [WR starting kit: develop new starting kit based on DIOT](#wr-starting-kit-develop-new-starting-kit-based-on-diot)                |     |  Y  |  Y  |  Y  |
| [WR starting kit: develop new starting kit based on CUTEWR-A7 + carrier](#wr-starting-kit-develop-new-starting-kit-based-on-cutewr-a7--carrier)  |     |  Y  |  Y  |  ?  |
| [WRS: Update RT-subsystem (SoftPLL) from LM32 to RiscV](#wrs-update-rt-subsystem-softpll-from-lm32-to-riscv)                    |     |  Y  |  Y  |  Y  |     |     |  Y  |
| [WRS: SoftPLL settings via command line and dot-config](#wrs-softpll-settings-via-command-line-and-dot-config)                  |     |  Y  |  Y  |  Y  |     |     |  Y  |
| [WRS: Support IRIG-B and NMEA input on GM mode](#wrs-support-irig-b-and-nmea-input-on-gm-mode)                          |     |  Y  |  Y  |  Y  |     |     |  Y  |
|[WRPC: expose leap second flags and value](#wrpc-expose-leap-second-flags-and-value)                               |     |  Y  |  Y  |     |     |     |  Y  |
|[WRPC: UTC time output](#wrpc-utc-time-output)                          |     |  Y  |     |     |     |     |  Y  |
|[WRPC: Add SoftPLL parameters change via CLI and config](#wrpc-add-softpll-parameters-change-via-cli-and-config)                 |     |     |  Y  |     |     |     |  Y  |
|[WRPC: Add IRIG-B output signal](#wrpc-add-irig-b-output-signal)        |     |  Y  |  Y  |     |     |     |  Y  |
|[WRPC: Add a configurable PWM output](#wrpc-add-a-configurable-pwm-output)|   |  Y  |  Y  |     |     |     |  Y  |
|[WR starting kit: DAC-less based on existing dev board](#wr-starting-kit-dac-less-based-on-existing-dev-board)                  |     |  Y  |  Y  |     |
|[WR-NIC from META: Add WRPC support](#wr-nic-from-meta-add-wrpc-support)|     |  Y  |  Y  |  Y  |
|[WR-NIC from META: Integration with Corundum](#wr-nic-from-meta-integration-with-corundum)                            |     |  Y  |  Y  |  Y  |
|[WR-NIC from META: make into WR Starting Kit](#wr-nic-from-meta-make-into-wr-starting-kit)                            |     |  Y  |  Y  |  Y  |
|[WRS: Develop CLI similar to other commercial managed switches](#wrs-develop-cli-similar-to-other-commercial-managed-switches)          |     |     |     |  Y  |
|[WRS: dump the current configuration back to dot-config](#wrs-dump-the-current-configuration-back-to-dot-config)                 |     |     |     |  Y  |
|[WRS: Implementation of In-situ calibration according to IEEE1588f](#wrs-implementation-of-in-situ-calibration-according-to-ieee1588f)      |     |     |     |  Y  |
|[WRS: implement MIB for IEEE1588f](#wrs-implement-mib-for-ieee1588f)    |     |     |     |  Y  |
|[WRPC: Add GTY support for WR](#wrpc-add-gty-support-for-wr)            |     |  Y  |     |     |
|[WRSv4: boot procedure](#wrsv4-boot-procedure)                          |     |     |     |  Y  |
|[WRSv4: update procedure](#wrsv4-update-procedure)                      |     |     |     |  Y  |
|[WRPC: Improve dual-ports support](#wrpc-improve-dual-ports-support)    |     |  Y  |  Y  |     |
|[WRS: Add web interface](#wrs-add-web-interface)                        |     |     |     |  Y  |
|[WRS: investigate WRS self calibration using DDMTD](#wrs-investigate-wrs-self-calibration-using-ddmtd)                      |     |  Y  |  ?  |     |




# Projects details

## WRPC: v5.0 support for all boards for which v4.2 is supported
### Description:
Some of the designs are not updated to WRPC v5.0. This project will contain the list of projects that needs the uplift of WRPC to v5.0.

### List of projects not updated to WRPC v5.0
TBD

## WR starting kit: update the current version based on SPECs
### Description:
The WR Starting Kit has been designed to test and check if the White Rabbit technology meets users requirements and to ease the understanding on what one can do with it and how to integrate it in own project. It is the easiest way to get acquainted with White Rabbit technology.
The WR Starting Kit uses two so-called WR Nodes, each being a SPEC with fmc-dio-5chttla FMC 5-channel Digital I/O module. Each WR Node implements basic operations such as input timestamping or programmable output pulse generation.
Additionally, specific software and gateware layers allow to use it as a standard network interface card implementing the White Rabbit functionality. Network packets with accurate time-stamping information are generated and timestamped at the hardware level to achieve the highest accuracy.

Currently the WR Starting Kit is supported for Ubuntu LTS 16.04.6 (kernel v.4.15.9) and Ubuntu LTS 18.04.2 (kernel v.4.18.0-17). The purpose of a project is to port wr-starting-kit to a newer kernels (in particular one running in Alma 9).

The current version of wr-starting-kit uses drivers that diverged from the officially supported versions.
In this project there are two possible approaches:
- uplift the used software to be compatible with the newer kernels
- use the latest versions of supported software (supported by a newer kernels) and assemble it into new version of wr-starting-kit

The first part of a project is to study which approach requires less effort.
The second part of the project is to implement the recommended approach


Additional information:
- WR Starting kit wiki/page: [wr-starting-kit wiki page](https://ohwr.org/project/wr-starting-kit/-/wikis/home)
- WR Starting kit manual: [wr-starting-kit.pdf](https://ohwr.org/project/wr-starting-kit/-/wikis/uploads/e97b42b1911c91d18c3d4631b1c12de2/wr-starting-kit.pdf)
- High-level repo: https://ohwr.org/project/wr-starting-kit/-/tree/wr-starting-kit-v3.1?ref_type=tags (tag wr-starting-kit-v3.1)
- SW/HDL code
  - https://gitlab.cern.ch/cohtdrivers/coht-vic
  - https://ohwr.org/project/fmc-dio-5chttla
  - https://ohwr.org/project/spec-sw
  - https://ohwr.org/project/wr-nic
  - https://ohwr.org/project/wrpc-sw
  - https://ohwr.org/project/wr-cores


Needed HW:
- 2xSPEC
- 2xDIO FMC
- fiber, 2xSFP, signal cabling
- oscilloscope (for testing)

### Deliverables:
- Part 1: Recommend best update strategy
- Part 2:
  - Make wr-starting-kit working with kernels:
    - 5.14 (alma 9)
    - 5.15 (ubuntu 22.04 LTS)
    - 6.8 (ubuntu 24.04 LTS)
  - Use latest version of WRPC (v5.0)
  - Port software (like wr-dio-cmd mentioned in wr-starting-kit.pdf)
  - Update wr-starting-kit documentation if needed





## WR starting kit: develop new starting kit based on DIOT
To be decided if needed. Maybe it is better to use another platform like: [WR starting kit: develop new starting kit based on CUTEWR-A7 + carrier](#wr-starting-kit-develop-new-starting-kit-based-on-cutewr-a7--carrier).
### Description:
Port WR starting kit to a new platform - DIOT SCB

### Deliverables:
- Port WR-starting-kit to DIOT SCB
- Update wr-starting-kit documentation

## WR starting kit: develop new starting kit based on CUTEWR-A7 + carrier
### Description:
Port WR starting kit to a new platform - CUTEWR-A7 + carrier developed by Grzegorz Kasprowicz

### Dependencies
This project requires the finish of:
- [WR starting kit: update the current version based on SPECs](#wr-starting-kit-update-the-current-version-based-on-specs)

### Deliverables:
- Port WR-starting-kit to CUTE-WR with carrier
- Update wr-starting-kit documentation


## WRS: Update RT-subsystem (SoftPLL) from LM32 to RiscV
### Description:
The firmware for White Rabbit Switch v3 (WRS) uses instance of LM32 (as soft-CPU) inside FPGA which runs SoftPLL. The aim of this project is to replace LM32 SoftCPU with RISC-V implementation (uRV).
LM32 memory is mapped in the ARM memory (Main CPU in WRS), RISC-V uses indirect addressing.

Similar task has been already performed for WRPCv5 and WRSv4 both might be useful for this project.

Additional information:
- HDL for WRSv3 https://ohwr.org/project/wr-switch-hdl/ branch master
- Software for soft-CPU https://ohwr.org/project/wrpc-sw (target _wr-switch_)
- Software for main ARM CPU on WRSv3 https://ohwr.org/project/wr-switch-sw/ branch master
- HDL for WRSv4 https://ohwr.org/project/wr-switch-hdl/ branch wrs-v4-dev
- Software for main ARM CPU on WRSv4 https://ohwr.org/project/wr-switch-sw/ branch wrs-v4-dev
- HDL for WRPCv5: https://ohwr.org/project/wr-cores tag wrpc-v5.0
- SW for soft-CPU of WRPCv5: https://ohwr.org/project/wrpc-sw tag wrpc-v5.0

Needed HW:
- WRSv3
- WR timing source (for testing)

### Deliverables:
- Replace LM32 soft-CPU in WRS with RISC-V implementation (HDL)
- Confirm that software in soft-CPU works on RISC-V (the software is already ported)
- Change software (wrsw_hal, wrs_shmem_dump and snmpd) running in the main CPU (ARM) to be able to communicate with RISC-V soft-CPU (instead of LM32)
- Update documentation

Needed HW:
- WRSv3
- WR timing source (for testing)

## WRS: SoftPLL settings via command line and dot-config
### Description
In White Rabbit Switch, add support for configuring the parameters of SoftPLL (running in soft-CPU) from Linux (main ARM CPU): dot-config at startup or command line interface at runtime.
To be defined which exactly parameters should be changeable (at minimum ki and pi from the spll_pi_t structure in https://ohwr.org/project/wrpc-sw/-/blob/master/softpll/spll_common.h?ref_type=heads).

For runtime configuration, the implementation should add the new parameters (one for each configurable SoftPLL parameter) to _wrsw_hal_conf_ tool. It should then make IPC call (using _minipc_) to HAL daemon (_wrsw_hal_), which would pass the request to soft-CPU in FPGA.

The reading of the current values of the parameters that can be changed shall be done via _wrs_shmem_dump_ tool.
It needs to extend the structure exported from SoftPLL and updates in
_wrs_shmem_dump_.

Additional information:
- Software for WRPC https://ohwr.org/project/wrpc-sw
- Software for WRS (main ARM CPU) https://ohwr.org/project/wr-switch-sw/

Needed HW:
- WRSv3
- WR timing source (for testing)

### Deliverables:
- Support of new parameters in _Kconfig_
- Support of new parameters in _wrsw_hal_ (for startup configuration)
- Support of new parameters in _wrsw_hal_conf_ (for runtime configuration)
- Support of new IPC requests in _wrsw_hal_
- Support of changing parameters in SoftPLL (exact list to be specified)
- Extend _wrs_shmem_dump_ to be able to read the SoftPLL parameters

## WRS: Support IRIG-B and NMEA input on GM mode
### Description
Add support of IRIG-B and NMEA input for White Rabbit Switch when in Grand Master mode. To be checked, but probably the IRIG-B and NMEA signals (one at the time, selected by configuration) could be provided by management RS232 interface in the back of WRS.

Potentially, this project can be split into two projects, 

Additional information:
- https://en.wikipedia.org/wiki/NMEA_0183
- https://en.wikipedia.org/wiki/IRIG_timecode
- HDL for WRS https://ohwr.org/project/wr-switch-hdl/
- Software for soft-CPU https://ohwr.org/project/wrpc-sw (target _wr-switch_)
- Software for main ARM CPU https://ohwr.org/project/wr-switch-sw/

Needed HW:
- WRSv3
- source of IRIG-B signal
- source of NMEA signal

### Deliverables:
- Propose a way how to provide IRIG-B and NMEA to WRS
- Study possible architecture of implementations (FPGA, soft-CPU, main ARM CPU)
- Implement IRIG-B input in WRS
- Implement NMEA input in WRS
- Update documentation

## WRPC: expose leap second flags and value
### Description
The current version of WRPC (v5.0) does not export to from SW to HDL the current leap second value nor the information about approaching negative/positive leap second. At this moment such information is available inside the WRPC software. The aim of this project is to add to WRPC a new HDL interface with this information, i.e.:
    leap_second_value_o       : in std_logic_vector(15 downto 0);
    leap_second_flag_59_o     : in std_logic;
    leap_second_flag_61_o     : in std_logic;
    leap_second_flag_valid_o  : in std_logic


Needed HW:
- Platform compatible with WRPC (e.g. SPEC)
- WR timing source (for testing)

### Deliverables:
- Add exposure of leap second value from WRPC in HDL
- Add exposure of leap second 59/60 flags from WRPC in HDL
- Update documentation

## WRPC: UTC time output
### Description
The current version of WRPC (v5.0) provides only value of TAI seconds since epoch (tm_tai_o : out std_logic_vector(39 downto 0);) Add interface with the value of UTC seconds since epoch, i.e., taking into account leap seconds, see [WRPC: expose leap second flags and value](#WRPC-expose-leap-second-flags-and-value)

Needed HW:
- Platform compatible with WRPC (e.g. SPEC)
- WR timing source (for testing)

### Deliverables:
- Add exposure of the value of UTC seconds from WRPC in HDL
- Add handling of leap-seconds event.
- Update documentation

## WRPC: Add SoftPLL parameters change via CLI and config
### Description
In WRPC (v5.0), add support for configuring the parameters of SoftPLL in runtime (in soft-CPU);
The initialization values shall be provided in the Kconfig, runtime changes shall be done via CLI command available on WRPC console.

The new configuration item in Kconfig XXX shall enable cli command YYY and allow to set new configuration items with initialization values:
XXX for XXX

To be defined which exactly parameters should be changeable (at minimum ki and pi from the spll_pi_t structure in https://ohwr.org/project/wrpc-sw/-/blob/master/softpll/spll_common.h?ref_type=heads).


Additional information:
- Software for WRPC https://ohwr.org/project/wrpc-sw
- Similar to  [WRS: SoftPLL settings via command line and dot-config](#wrs-softpll-settings-via-command-line-and-dot-config)

Needed HW:
- Platform compatible with WRPC (e.g. SPEC) with output signal(s)
- WR timing source (for testing)


### Deliverables:
- Add cli command to WRPC that will allow a change of softPLL parameters.
- Add number of configuration items to Kconfig to specify initialization values for SoftPLL
- Update documentation


## WRPC: Add IRIG-B output signal
### Description

 Add IRIG-B output signal to WRPC.

Related to [WRS: Support IRIG-B and NMEA input on GM mode](#wrs-support-irig-b-and-nmea-input-on-gm-mode) 

Additional information:
- https://en.wikipedia.org/wiki/IRIG_timecode
- HDL for WRPC https://ohwr.org/project/wr-cores
- Software for WRPC https://ohwr.org/project/wrpc-sw

Needed HW:
- Platform compatible with WRPC (e.g. SPEC)
- WR timing source (for testing)
- way to verify IRIG-B signal

## WRPC: Add a configurable PWM output
### Description
In WRPCv5, using OSERDES, add generation of a 5MHz, 20MHz and 1MHz frequencie from the _wr_clk_, with a possible different duration occupancy (for example a fixed high level duration with either 10MHz or 5MHz).
- In HDL: Re-use and adapt module from the WR switch which generates 10 MHz and can be configured to generate other frequencies: https://ohwr.org/project/wr-switch-hdl/-/blob/proposed_master/modules/wrsw_rt_subsystem/xwrsw_gen_10mhz.vhd.
- In SW: add commands to CLI commands to configure the clock


Additional information:
- HDL for WRPC https://ohwr.org/project/wr-cores
- Software for WRPC https://ohwr.org/project/wrpc-sw

Needed HW:
- Platform compatible with WRPC (e.g. SPEC) with at least one I/O
- WR timing source (for testing)

### Deliverables:
- Add a module to WRPC which generates the signal
- Add a cli command and a configuration item into Kconfig that will define if this feature is enabled, provide target derived frequency clock add duration factor


## WR starting kit: DAC-less based on existing dev board
### Description
Include into WR-starting-kit a DAC-less WR implementation on an existing dev board (e.g., ZCU102).

More:
- DAC-less WR: [Presentation "Over the horizon" Tomasz Włostowski, 12th White Rabbit Workshop](https://ohwr.org/project/white-rabbit/-/wikis/uploads/aa61dc58e62f583d491e32dc8ff1d776/wr_workshop_2022.pdf)


## WR-NIC from META: Add WRPC support
### Description


## WR-NIC from META: Integration with Corundum
### Description


## WR-NIC from META: make into WR Starting Kit
### Description
Requires [WR-NIC from META: Add WRPC support](#wr-nic-from-meta-add-wrpc-support)

## WRS: Develop CLI similar to other commercial managed switches
### Description

As today the configuration and user experience of WR Switch differs a lot from what is available on the market from other vendors making network equipment.
WR switch is configured and managed more like a Linux system than a professional managed switch.
By introducing on WRS a CLI similar to other solutions available on the market it would be possible to reduce the learning effort for technicians managing WR and other types of network equipment.


### Deliverables
Step 1:
- Make a study of available CLIs on equipment from major vendors that are available on the market
Step 2:
- Make a proposal for a new CLI for WRS
Step 3:
- Implement proposal

## WRS: dump the current configuration back to dot-config
### Description

In WR switch some configuration can be changed in run-time (PPSi, wrsw_hald and SoftPLL (when [WRS: SoftPLL settings via command line and dot-config](#wrs-softpll-settings-via-command-line-and-dot-config) is implemented)). It might be useful to provide a functionality to dump the runtime configuration back to dot-config.

For HAL it could be done:
- `wrsw_hal_conf` is called to dump its configuration to _dot-config_ compatible configuration items. These items are added at the end of _dot-config_
- `conf` is called to normalize the _dot-config_

For PPSi it could be done:
- `ppsi_conf` is called to dump its configuration to `/etc/ppsi.conf` (or other file provided as parameter)
- `/etc/ppsi.conf` is converted to _dot-config_ compatible configuration items. These items are added at the end of _dot-config_
- `conf` is called to normalize the _dot-config_

Additional information:
- Software for WRS https://ohwr.org/project/wr-switch-sw/
- PPSi https://ohwr.org/project/ppsi

Needed HW:
- WRSv3 for testing/development (some developments could be done without WRS)

### Deliverables:

## WRS: Implementation of In-situ calibration according to IEEE1588f
### Description

TDB

## WRS: implement MIB for IEEE1588f
### Description
Requires [WRS: Implementation of In-situ calibration according to IEEE1588f](#wrs-implementation-of-in-situ-calibration-according-to-ieee1588f)


## WRPC: Add GTY support for WR
### Description
As today White Rabbit is not supported for GTY transceivers. The aim of this project is to write a HDL wrapper for GTY transceiver to provide WR support.

Additionally, this project could add a support of a board with FPGA with GTY transceiver for WRPC (HDL and SW; minor task).

[The list of already supported transceivers](https://ohwr.org/project/wr-cores/-/wikis/wrpc-core)
### Deliverables:
- Add support of GTY transceiver to WRPC
- Add a support of a board with FPGA with GTY transceiver for WRPC (HDL and SW; minor task).

## WRSv4: boot procedure
### Description
The new WR switch v4 is based on Zynq Ultra scale platform. The aim of this project is to study the boot sequence that can be used for WRSv4.

### Requirements
- Person experienced with Zynq US

### Deliverables:
- Proposal of the boot procedure for WRSv4
- Implementation of the proposal

This project could potentially be divided into two parts.

## WRSv4: update procedure
### Description

Related to [WRSv4: boot procedure](#wrsv4-boot-procedure).

### Requirements
- Person experienced with Zynq US

### Deliverables:
- Proposal of the firmware procedure
- Implementation of the proposal

This project could potentially be divided into two parts.

## WRPC: Improve dual-ports support
### Description


## WRS: Add web interface
### Description
White Rabbit Switch at the moment has implemented web-interface. However, it rust over the time and number of security vulnerabilities were discovered. As a result, the current version of the web-interface is disabled by default and users are discouraged to use it. For sure the web-interface, even with a basic functionality, would make WRS more friendly for new users.

Hopefully this could re-use some parts of the WRS management system.

### Deliverables:
- Proposal of the new web-interface
- Implement the new web-interface

This project could potentially be divided into two parts.

## WRS: investigate WRS self calibration using DDMTD
### Description


